/*Тестовое задание для вакансии PHP прогаммист от компании МетаДизайн
Выполнил: Черноусов Денис
Красноярск 2015
*/
//Подключите заранее jQuery

const AJAX_URL = "controller.php"

$(function() {

	console.log("jQ агрузилось");

	$("input#date").mask("99.99.9999",{placeholder:"__.__.____"});
	$("input#phone").mask("+7(999)999-99-99",{placeholder:"+7(___)___-__-__"});

	$("#registration").validate({
	       rules:{
	            name:{
	                required: true
	            },
	            surname:{
	                required: true
	            },
	            phone:{
	                required: true
	            },
	            date:{
	            	required: false,
	            	date: true
	            }
	       },
	       messages:{

	            name:{
	                required: "Это поле обязательно для заполнения"
	            },
	            surname:{
	                required: "Это поле обязательно для заполнения"
	            },
	            phone:{
	                required: "Это поле обязательно для заполнения"
	            },
	            date:{
	            	date: "Введи корректную дату рождения"
	            }
	       }

	});

	$("#login_form").validate({
	       rules:{
	            login:{
	                required: true,
	                minlength: 3,
	                maxlength: 20
	            },
	            pass:{
	                required: true,
	                minlength: 5,
	                maxlength: 20
	            }
	       },
	       messages:{

	            login:{
	                required: "Заполните логин",
	                minlength: "Не менее 3 символов"
	            },
	            pass:{
	                required: "Заполните пароль",
	                minlength: "Не менее 5 символов"
	            }
	       }

	});

});


function reg_form(form_){

	//$("#registration").submit();
	
	if ( $("#registration").valid() ) 
	{
		print_log('Успешно!');
		form_ = document.getElementById('registration');

		var user_fields = { 
			name : form_.name.value,
			surname : form_.surname.value,
			date : form_.date.value,
			company : form_.company.value,
			position : form_.position.value,
			phone : form_.phone.value
		 };
		
		// print_log(user_fields);

		$.post(
			AJAX_URL,
			{
				method: "reg",
				fields: user_fields
			},
			function(data){
				if (data['errors']) {
					print_log(data['errors']);
					$('#reg_window .ok').text('').hide(400);
					$('#reg_window .errors').text( data['errors'] ).show(400);
				};

				if (data['ok']) {
					print_log(data['ok']);
					$('#reg_window .errors').text('').hide(400);
					$('#reg_window .ok').text(data['ok']).show(400);
					$("#registration input").val('');
					//$('#reg_window').delay( 800 ).modal('hide')
				};

			},
			'JSON'
		);

	}

	//print_log(form_);
}
function login_form(form_){
	
	if ( $("#login_form").valid() ) 
	{
		form_ = document.getElementById('login_form');

		var login_fields = { 
			login : form_.login.value,
			pass : form_.pass.value
		 };
		
		//print_log(login_fields);

		$.post(
			AJAX_URL,
			{
				method: "login",
				fields: login_fields
			},
			function(data){
				if (data['errors']) {
					print_log(data['errors']);
					$('#login_window .ok').text('').hide(400);
					$('#login_window .errors').text( data['errors'] ).show(400);
				};

				if (data['ok']) {
					print_log(data['ok']);
					$('#login_window .errors').text('').hide(400);
					$('#login_window .ok').text(data['ok']).show(400);
					$("#login_form input").val('');
					$('#login_window').modal('hide');
					location.reload();
				};

			},
			'JSON'
		);
	}
}
function logout(){
	$.post(
			AJAX_URL,
			{
				method: "logout"
			},
			function(data){
				location.reload();
			},
			'JSON'
		);
}
function admin_panel(){

	$.post(
			AJAX_URL,
			{
				method: "admin_panel"
			},
			function(data){
				var panel = $('#admin_panel');
				panel.find('.modal-body').html(data);
				panel.modal();
			}
		);

	

}


function read_users(){

	$.get(
		AJAX_URL,
		{
			method: "read_users"
		},
		print_log
	);

}










/*Вывод в консоль */
function print_log(data){
	 console.log(data);
}
/*Вывод на экран*/
function print_alert(data)
{
	alert(data);
}