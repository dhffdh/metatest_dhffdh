<?php 
session_start();?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Тестовое задание от МетаДизайн</title>
	
	<link rel="stylesheet" type="text/css" href="/static/style.css">
	<link rel="stylesheet" type="text/css" href="/static/bootstrap/css/bootstrap.min.css">


	<script type="text/javascript" src="/static/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="/static/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/static/jquery.maskedinput.min.js"></script>
	<script type="text/javascript" src="/static/jquery.validate.min.js"></script>
  	<script type="text/javascript" src="/static/script.js"></script>


</head>
<body>
<header>

	<nav class="navbar navbar-default">
	  <div class="container">
		<ul class="nav nav-pills">
		  <li role="presentation" class="active"><a href=""><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
		  <li role="presentation"><a href="#" data-toggle="modal" data-target="#readme">Задание</a></li>

<? if ($_SESSION['admin']=='true') {?>
		<li role="presentation"><a href="#" onclick="admin_panel();" data-target="#admin_panel">Админ панель!</a></li>
		<li role="presentation"><a href="#" onclick="logout();">Выйти</a></li>
	<?}else{?>
		<li role="presentation"><a href="#" data-toggle="modal" data-target="#login_window">Войти</a></li>
	<?}?>

		<li role="presentation"><a href="#" data-toggle="modal" data-target="#reg_window"> <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Регистрация</a></li>
		

		</ul>	
	  </div>
	</nav>


</header>


<div class="container">
		<div class="jumbotron">
			<?php
			echo "<h1>Welcome!</h1>";
			?>
		  <p>...</p>
		  <p><a class="btn btn-primary btn-lg" href="#" data-toggle="modal" data-target="#reg_window" role="button">Регистрация</a></p>
		</div>
		
		<div  class="row">

		</div>


	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
	  Launch demo modal
	</button>

</div>



<footer>
</footer>

<div class="">

	<div class="modal fade" id="readme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_1">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        	<span aria-hidden="true">&times;</span>
	        </button>
	        <h3 class="modal-title" id="myModalLabel_1">Тестовое задание PHP разработчика</h3>
	      </div>
	      <div class="modal-body">

	    		<h2 id="markdown-header-">Цель задания</h2>
				<p>Выявить уровень владения php/js.</p>
				<p>Особое внимание следует уделить как решению поставленной задачи, так и к стилю написания кода, аспектам безопасности, возможному перспективному росту приложения.
					Нам не важно, будет ли это до конца рабочая система, главное понять архитектуру и ход ваших мыслей при разработке.
					Также, не следует заострять внимание на визуальной составляющей задания. В проекте присутствует фреймворк bootstrap 3. Все макеты желательно делать на его основе.</p>
				<h2 id="markdown-header-_1">Инструменты разработки</h2>
				<ul>
					<li>серверная часть</li>
					<li>
						<ul>
							<li>php 5.5</li>
						</ul>
					</li>
					<li>клиентская часть</li>
					<li>
						<ul>
							<li>jquery 1.11</li>
						</ul>
					</li>
					<li>
						<ul>
							<li>bootstrap 3</li>
						</ul>
					</li>
				</ul>
				<h2 id="markdown-header-_2">Описание задания</h2>
				<p>Создать приложение, позволяющее проходить регистрацию на вымышленном событии путем заполнения формы. Форма должна работать без перезагрузки страницы (ajax).
					Поля формы:
				</p>
				<ol>
					<li>имя</li>
					<li>фамилия</li>
					<li>дата рождения</li>
					<li>компания</li>
					<li>должность</li>
					<li>телефон</li>
				</ol>
				<p>Поля 1,2,6 являются обязательными для заполнения. Поле телефон должно содержать только числа и знак "+". Поле дата рождения соответствовать маске dd.mm.yyyy
				</p>
				<p>После отправки формы, на стороне сервера данные повторно проверяются и записываются в CSV файл. В случае если пользователь с введенным телефоном уже имеется - система сообщает об этом.
				</p>
				<p>Помимо формы регистрации разработать страницу для администратора, которая позволяет просматривать список зарегистрированных пользователей в удобном виде, а также выгружать данные с сайта.
					Страница администратора должна быть закрыта от свободного доступа.
				</p>


	      </div>

	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
	      </div>
	      
	    </div>
	  </div>
	</div>


	

	<div class="modal fade" id="reg_window" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        	<span aria-hidden="true">&times;</span>
	        </button>
	        <h3 class="modal-title" id="myModalLabel">Регистрация</h3>
	      </div>
	     <div class="modal-body">
	
			<form action="" id="registration" method="POST">
				
				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon1">Имя</span>
				  <input id="name" name="name" type="text" class="form-control" placeholder="Имя" aria-describedby="basic-addon1" required="required">
				</div>
				
				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon2">Фамилия</span>
				  <input id="surname" name="surname" type="text" class="form-control" placeholder="Фамилия" aria-describedby="basic-addon2" required="required">
				</div>

				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon3">Дата рождения</span>
				  <input id="date" name="date" type="text" class="form-control" placeholder="Дата рождения" aria-describedby="basic-addon3">
				</div>

				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon4">Компания</span>
				  <input id="company" name="company" type="text" class="form-control" placeholder="Компания" aria-describedby="basic-addon4">
				</div>

				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon5">Должность</span>
				  <input id="position" name="position" type="text" class="form-control" placeholder="Должность" aria-describedby="basic-addon5">
				</div>

				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon">Телефон</span>
				  <input id="phone" name="phone" type="text" class="form-control" placeholder="Телефон" aria-describedby="basic-addon6" required="required"> 
				</div>
			
				<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
			        <button type="button" method="POST" class="btn btn-primary" onclick="reg_form(this.parentNode);">Регистрация</button>
			    </div>
			</form>

			<div id="ok" class="ok alert alert-success hide1" role="alert">Успешно</div>
			<div id="errors" class="errors alert alert-danger hide1" role="alert">Ошибка</div>

	    </div>
	    </div>
	  </div>
	</div>
		
	<div class="modal fade" id="login_window" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        	<span aria-hidden="true">&times;</span>
	        </button>
	        <h3 class="modal-title" id="myModalLabel">Вход</h3>
	      </div>
	     <div class="modal-body">
	
			<form action="" id="login_form" method="POST">
				
				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon1">Логин</span>
				  <input id="login" name="login" type="text" class="form-control" placeholder="login" aria-describedby="basic-addon1">
				</div>
				
				<div class="input-group">
				  <span class="input-group-addon" id="basic-addon2">Пароль</span>
				  <input id="pass" name="pass" type="password" class="form-control" placeholder="password" aria-describedby="basic-addon2">
				</div>
			
				<div class="modal-footer">
			        <button type="button" method="POST" class="btn btn-success" onclick="login_form(this.parentNode);">Войти</button>
			     </div>
			</form>

			<div id="ok" class="ok alert alert-success hide1" role="alert">Успешно</div>
			<div id="errors" class="errors alert alert-danger hide1" role="alert">Ошибка</div>

	    </div>
	    </div>
	  </div>
	</div>



	<div class="modal fade" id="admin_panel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        	<span aria-hidden="true">&times;</span>
	        </button>
	        <h3 class="modal-title" id="myModalLabel">Админ панель</h3>
	      </div>
	     <div class="modal-body">
	

	    </div>
	    </div>
	  </div>
	</div>

</div>


</body>
</html>


