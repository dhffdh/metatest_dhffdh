<?php 
session_start();


define("USERS_FILE", "users.csv"); #новость


class User{

/*
var user_fields = { 
	name : form_.name.value,
	surname : form_.surname.value,
	date : form_.date.value,
	company : form_.company.value,
	position : form_.position.value,
	phone : form_.phone.val
*/

	public $name		="";
	public $surname	="";
	public $date		="";
	public $company	="";
	public $position	="";
	public $phone		="";

	private $checked = false;

	function __construct( $params )
	{	
		$this->name = 			$params['name'];
		$this->surname = 		$params['surname'];
		$this->date = 			$params['date'];
		$this->company = 		$params['company'];
		$this->position = 		$params['position'];
		$this->phone = 			$params['phone'];
	}

	function get(){
		return $this;
	}

	function save(){
		if ($this->checked == false) {
			if (!$this->check_phone_coincidence()){
				return false;
			}
		}

		save_user_to_file($this);

		return true;
	}
	function validate(){
		$errors = false;
		if ($this->check_phone_coincidence() == false){
			$errors[] = "Пользователь с таким Номером телефона уже существует!!!";
			//return false;
		}

		return $errors;
	}
	private function check_phone_coincidence(){
		$this->checked = true;
		return check_users_on_phone( $this->phone );
	}
	public static function read_users()
	{
		read_users_from_file();
	}
}






#Функция сохранения данных $user в конец файла
function save_user_to_file($user)
{
	$fp = fopen(USERS_FILE, 'a');
	fputcsv($fp, (array)$user, "#");
	fclose($fp);
}

#Функция вывода всех данных с файла USERS_FILE
function read_users_from_file(){
	$row = 1;
	echo '<table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Дата Рождения</th>
            <th>Компания</th>
            <th>Должность</th>
            <th>Телефон</th>
          </tr>
        </thead>
        <tbody>';
	if (($handle = fopen(USERS_FILE, "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, "#")) !== FALSE) {
		echo "<tr>";
		echo '<th>',$row,'</th>';
		echo '<th>',$data[0],'</th>';
		echo '<th>',$data[1],'</th>';
		echo '<th>',$data[2],'</th>';
		echo '<th>',$data[3],'</th>';
		echo '<th>',$data[4],'</th>';
		echo '<th>',$data[5],'</th>';
    	$row++;
	    }
	    fclose($handle);
	}
	echo '</tbody></table>';

}


# Функция проверка файла на совпадение переданного номера с номерами в файле
function check_users_on_phone( $phone_ ){


	if (($handle = fopen(USERS_FILE, "r")) !== FALSE) {
	    while (($data = fgetcsv($handle, 1000, "#")) !== FALSE) {
	        $num = count($data);
	        $current_phone = $data[5];
	        //echo $current_phone;
	        if ($phone_ == $current_phone) {
	        	//echo "Совпадение";
	        	return false;
	        }
	    }
	    fclose($handle);
	}
	return true;
}





?>