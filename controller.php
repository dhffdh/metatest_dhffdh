<?
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once('classes.php');

if ( $_SERVER['REQUEST_METHOD'] == 'POST') {

	$method = $_POST['method'];
	switch ($method) {

		case 'reg':
			$fields = $_POST['fields'];
			
			$user = new User([
				'name'		=>	strip_tags(trim ($fields['name'] 		)),
				'surname'	=>	strip_tags(trim ($fields['surname'] 	)),
				'date'		=>	strip_tags(trim ($fields['date'] 		)),
				'company'	=>	strip_tags(trim ($fields['company'] 	)),
				'position'	=>	strip_tags(trim ($fields['position']	)),
				'phone'		=>	strip_tags(trim ($fields['phone'] 		)),
			]);

			//echo $fields;
			$result = array();
			
			$errors = $user->validate();
			if ( $errors == false ) {
				$result['ok'] = "Пользователь $user->name успешно сохранен";

				$user->save();

			}else{
				$result['errors'] = $errors;
			}
			echo json_encode( $result );
			break;


		case 'login':
			$login_fields = $_POST['fields'];
			$login = strip_tags(trim ($login_fields['login'] ));
			$pass = strip_tags(trim ($login_fields['pass'] ));


			$result = array();
			
			if ($login == 'admin' && $pass=='12345') {
				$errors = false;
			}else{
				$errors[] = "Неверный логин или пароль!";
			}
			
			if ( $errors == false ) {
				$result['ok'] = "Вы успешно вошли, $login";
				$_SESSION['admin'] = 'true';

			}else{
				$result['errors'] = $errors;
			}
			echo json_encode( $result );
			break;


		case 'logout':
			unset($_SESSION['admin']);
		echo json_encode( ['ok' => 'Успешно вышли!']);
			break;



		case 'admin_panel':
			if ($_SESSION['admin'] == 'true') {
				echo User::read_users();
			}else{
				echo "Ошибка доступа!!!";
			}
			
			break;


		default:
			echo "server: Method undefined!!!";
			break;
	}

}else{

	$method = $_GET['method'];

		switch ($method) {

			case 'read_users':
				echo User::read_users();
				break;


			default:
				echo "server: Method undefined!!!";
				break;
		}

}